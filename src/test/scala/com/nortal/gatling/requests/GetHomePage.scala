package com.nortal.gatling.requests

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import com.nortal.gatling.config.Config.app_url

object GetHomePage {
  val get_home = http("Get Home Page")
                    .get(app_url + "/")
                    .check(status is 200)
}