package com.nortal.gatling.simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

import com.nortal.gatling.config.Config._
import com.nortal.gatling.scenarios.ComputerDemoScenario

class BasicSimulation extends Simulation {

  val httpProtocol = http
    .baseUrl(app_url) // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")



  private val scn = ComputerDemoScenario.computerDemoScenario

  setUp(
      scn.inject(
        nothingFor(4 seconds),
        constantUsersPerSec(users) during (5 seconds)
        )
      ).protocols(httpProtocol)


}
